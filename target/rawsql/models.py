from django.db import models

# Create your models here.

from django.db import models


class MyModel(models.Model):
    my_text = models.CharField("My Text", max_length=200)
    my_date = models.DateTimeField("My Date")
    def __str__(self):
        return self.my_text