
from django.db import connection 
from django.http import HttpResponse, Http404
from django.shortcuts import render, redirect
from django.template import loader
from django import forms

from .models import MyModel

class MyForm(forms.Form):
    my_text = forms.CharField(help_text="enter my_text")
    my_date = forms.DateTimeField(help_text="enter my_date")

def myview(request, mymodel_id):
  # late imports confuse semgrep while resolving fully qualified function calls
  # todoruleid: python_django_rule-django-rawsql-used
  RawSQL('SELECT count(*) FROM rawsql_mymodel %s' % mymodel_id,[])


from django.db.models.expressions import RawSQL


def index(request):
    mymodels = MyModel.objects.order_by("-my_date")#[:5]
    template = loader.get_template("index.html")

    myform = MyForm(request.POST)
    myflag = False
    modelid = None
    not_contained_count = None

    template = "index.html"

    if request.method == "POST":
        myflag = True
        myform.is_valid()
        if myform.cleaned_data['my_text']!='':
            mymodel_text=myform['my_text'].value()

            mymodels = MyModel.objects.annotate(
              not_contained_count=RawSQL(
                  # ruleid: python_django_rule-django-rawsql-used
                  'SELECT count(*) FROM rawsql_mymodel WHERE my_text NOT LIKE "%s"' % mymodel_text,params=()
              )
            )
            mymodel= mymodels.get(pk=1)
            not_contained_count = mymodel.not_contained_count


            # template = "mymodel/detail.html"
            # mymodel_id=myform['my_text'].value()
            # return redirect('mymodels/'+myform['my_text'].value()+'//rawsql/')
            # return redirect('detail',mymodel_id=mymodel_id,path='empty')


    context = {
                "mymodels": mymodels,
                "form": myform,
                "myflag": myflag,
                "modelid": modelid,
                "not_contained_count": not_contained_count,
                }

    return render(request, template, context)

def detail_old(request, mymodel_id):
  # ruleid: python_django_rule-django-rawsql-used
  RawSQL('SELECT count(*) FROM rawsql_mymodel %s' % request,[])
  try:
      mymodel = MyModel.objects.get(pk=mymodel_id)
  except MyModel.DoesNotExist:
      raise Http404("Question does not exist")
  return render(request, "mymodel/detail.html", {"mymodel": mymodel})



def myfunction(arg1, arg2, art2=None):
    return None

#def detail_vulnerable_001(request, mymodel_id):
def detail(request, mymodel_id, path):
  #user_name = request.get('user_name')
  
  source01="this is a constant"
  source02="this is also a constant"
  source03=request.headers.get('User-Agent')
  source04=request.META.get('HTTP_IDCLIENT', "")
  source05=myfunction('HTTP_IDCLIENT', None)

  mymodel= MyModel.objects.get(pk=mymodel_id)
  mymodel_text=mymodel.my_text
  mymodel_not_contained_count=0

  # ruleid: python_django_rule-django-rawsql-used
  RawSQL('SELECT count(*) FROM rawsql_mymodel %s' % request,[])
  # ruleid: python_django_rule-django-rawsql-used
  RawSQL('SELECT count(*) FROM rawsql_mymodel %s' % mymodel_id,[])
  # ruleid: python_django_rule-django-rawsql-used
  RawSQL('SELECT count(*) FROM rawsql_mymodel %s' % path,[])

  # ok: python_django_rule-django-rawsql-used
  RawSQL('SELECT count(*) FROM rawsql_mymodel %s' % "XXXX",[])

  query='SELECT count(*) FROM rawsql_mymodel %s' % source02  
  # ok: python_django_rule-django-rawsql-used
  RawSQL(query,[])

  query='SELECT count(*) FROM rawsql_mymodel ' + source04
 # ruleid: python_django_rule-django-rawsql-used
  RawSQL(query,[])

  query='SELECT count(*) FROM rawsql_mymodel {}'.format(source04)
# ruleid: python_django_rule-django-rawsql-used
  RawSQL(query,[])

  param = 'hello'
  query=f'SELECT count(*) FROM rawsql_mymodel {param}'
# ok: python_django_rule-django-rawsql-used
  RawSQL(query,[])

  param = 'hello'
# ok: python_django_rule-django-rawsql-used
  RawSQL(f'SELECT count(*) FROM rawsql_mymodel {param}',[])

  # ok: python_django_rule-django-rawsql-used
  RawSQL('',[])

  RawSQL(
          # ruleid: python_django_rule-django-rawsql-used
          'SELECT count(*) FROM rawsql_mymodel WHERE my_text NOT LIKE "%s"' % mymodel_text,params=()
        )

  RawSQL(
          # ruleid: python_django_rule-django-rawsql-used
          'SELECT count(*) FROM rawsql_mymodel WHERE my_text NOT LIKE "%s"' % source03,params=()
        )

  sink="{}".format(source01)

  mymodels = MyModel.objects.annotate(
      not_contained_count=RawSQL(
          # ruleid: python_django_rule-django-rawsql-used
          'SELECT count(*) FROM rawsql_mymodel WHERE my_text NOT LIKE "%s"' % mymodel_text,params=()
        )
    )

  mymodels = MyModel.objects.annotate(
      not_contained_count=RawSQL(
          # ok: python_django_rule-django-rawsql-used
          'SELECT count(*) FROM rawsql_mymodel WHERE my_text NOT LIKE "%s"' % source05,params=()
        )
  )

  mymodel= mymodels.get(pk=mymodel_id)
  return render(request, "mymodel/detail.html", {"mymodel": mymodel})
  
  html = """
    <html>
        <body>
        MyModel ID is %s.   <br>
        Its text is %s.     <br>
        Its non_contained_count is %s. <br>
        </body>
    </html>""" % (mymodel_id, mymodel_text, mymodel.not_contained_count)
  return HttpResponse(html)