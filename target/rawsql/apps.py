from django.apps import AppConfig


class RawsqlConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'rawsql'
