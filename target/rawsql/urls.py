from django.urls import path

from . import views

urlpatterns = [
    path("", views.index, name="rawsql"),
    path("mymodels/<int:mymodel_id>/<path:path>", views.detail, name="detail"),
    # path("mymodels/<int:mymodel_id>/", views.detail, name="detail"),
]
